import Vue from "nativescript-vue";
const Browse = {
	template: `
      <Page>
        <ActionBar title="Browse"/>
        <StackLayout>
          <Label  @tap="$modal.close"  text="Close" />
        </StackLayout>
      </Page>
  `,
};
const Items = {
	template: `
  <Page>
  <ActionBar title="Home"/>
  <ActionBar title="My App">
    <ActionItem @tap="onTapEdit"
      v-show="!isEditing"
      ios.systemIcon="2" ios.position="right"
      android.systemIcon="ic_menu_edit" />
    <ActionItem @tap="onTapSave"
      v-show="isEditing"
      ios.systemIcon="3" ios.position="right"
      android.systemIcon="ic_menu_save" />
    <ActionItem @tap="onTapCancel"
      v-show="isEditing"
      ios.systemIcon="1"
      android.systemIcon="ic_menu_close_clear_cancel" />
      <NavigationButton text="Go back" android.systemIcon="ic_menu_back" @tap="goBack" />
  </ActionBar>
  <ScrollView>
  
      <StackLayout class="home-panel">
          <!--Add your page content here-->
          <Label textWrap="true" text="Play with NativeScript!" class="h2 description-label" />
          <Label textWrap="true" text="Write code in the editor or drag and drop components to build a NativeScript mobile application." class="h2 description-label" />
          <Label textWrap="true" text="Scan the QR code with your mobile device and watch the changes sync live while you play with the code." class="h2 description-label" />
      </StackLayout>
      <AbsoluteLayout>
        <Label text="10,10" left="10" top="10" width="100" height="100" backgroundColor="#43b883"/>
        <Label text="120,10" left="120" top="10" width="100" height="100" backgroundColor="#43b883"/>
        <Label text="10,120" left="10" top="120" width="100" height="100" backgroundColor="#43b883"/>
        <Label text="120,120" left="120" top="120" width="100" height="100" backgroundColor="#43b883"/>
      </AbsoluteLayout>
      <DockLayout stretchLastChild="true" backgroundColor="#3c495e">
      <Label text="left" dock="left" width="40" backgroundColor="#43b883"/>
      <Label text="top" dock="top" height="40" backgroundColor="#289062"/>
      <Label text="right" dock="right" width="40" backgroundColor="#43b883"/>
      <Label text="bottom" dock="bottom" height="40" backgroundColor="#289062"/>
    </DockLayout>
    <FlexboxLayout backgroundColor="#3c495e">
      <Label text="first" width="70" backgroundColor="#43b883"/>
      <Label text="second" width="70" backgroundColor="#1c6b48"/>
      <Label text="third" width="70" backgroundColor="#289062"/>
    </FlexboxLayout>
    
  </ScrollView>
  <FlexboxLayout flexDirection="column" backgroundColor="#3c495e">
    <Label text="first" height="70" backgroundColor="#43b883"/>
    <Label text="second" height="70" backgroundColor="#1c6b48"/>
    <Label text="third" height="70" backgroundColor="#289062"/>
    <DatePicker  />
    <Image style="background:red" src="https://art.nativescript-vue.org/NativeScript-Vue-White-Green.png" stretch="none" />
  </FlexboxLayout>
</Page>
  `,
};
const Search = {
	template: `
      <Page>
        <ActionBar title="Search"/>
        <StackLayout>
          <Button @tap="$modal.close" text="Close" />
        </StackLayout>
      </Page>
  `,
};
const App = {
	components: {
		Items,
		Browse,
		Search,
	},

	template: `
    <BottomNavigation>
      <TabStrip>
        <TabStripItem>
          <Label text="Home"></Label>
        </TabStripItem>
        <TabStripItem>
          <Label text="Browse"></Label>
        </TabStripItem>
        <TabStripItem>
          <Label text="Search"></Label>
        </TabStripItem>
      </TabStrip>

      <TabContentItem>
        <Frame id="homeTabFrame">
          <Items />
        </Frame>
      </TabContentItem>

      <TabContentItem>
        <Frame id="browseTabFrame">
          <Browse />
        </Frame>
      </TabContentItem>

      <TabContentItem>
        <Frame id="searchTabFrame">
          <Search />
        </Frame>
      </TabContentItem>
    </BottomNavigation>
  `,
};

new Vue({
	render: (h) => h(App),
}).$start();
