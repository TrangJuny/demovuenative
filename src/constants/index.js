import sss from "./sss";
import regex from "./regex";

export const SSS = sss;
export const REGEX_PATTERNS = regex;
