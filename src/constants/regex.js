const UUID_V4 = /^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i;
const CONTAINS_CAPITAL = /[A-Z]/;
const CONTAINS_SPECIAL = /[!@#$%^&*(),.?"'/;\\=+\-_~`:{}|<>]/;
const CONTAINS_ALPHA_AND_NUM = /^(?=.*[a-zA-Z])(?=.*[0-9])/;

export default {
  UUID_V4,
  CONTAINS_CAPITAL,
  CONTAINS_SPECIAL,
  CONTAINS_ALPHA_AND_NUM,
};
