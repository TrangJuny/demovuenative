import { helpers } from "vuelidate/lib/validators";
import { REGEX_PATTERNS } from "../../../constants";

export const containsAlphaAndNum = helpers.regex(
  "containsAlphaAndNum",
  REGEX_PATTERNS.CONTAINS_ALPHA_AND_NUM
);

export const containsCapital = helpers.regex(
  "containsCapital",
  REGEX_PATTERNS.CONTAINS_CAPITAL
);

export const containsSpecial = helpers.regex(
  "containsSpecial",
  REGEX_PATTERNS.CONTAINS_SPECIAL
);
