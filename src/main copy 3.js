import Vue from "nativescript-vue";
const Browse = {
	template: `
    <Frame>
      <Page>
        <ActionBar title="Browse"/>
        <StackLayout>
          <Label  text="Close" />
        </StackLayout>
      </Page>
    </Frame>
  `,
};
const Items = {
	template: `
    <Frame>
      <Page>
        <ActionBar title="Items"/>
        <StackLayout>
          <AbsoluteLayout backgroundColor="#3c495e">
            <Label text="10,10" left="10" top="10" width="100" height="100" backgroundColor="#43b883"/>
            <Label text="120,10" left="120" top="10" width="100" height="100" backgroundColor="#43b883"/>
            <Label text="10,120" left="10" top="120" width="100" height="100" backgroundColor="#43b883"/>
            <Label text="120,120" left="120" top="120" width="100" height="100" backgroundColor="#43b883"/>
          </AbsoluteLayout>
          <Button @tap="$modal.close" text="Close" />
        </StackLayout>
      </Page>
    </Frame>
  `,
};
const Search = {
	template: `
    <Frame>
      <Page>
        <ActionBar title="Search"/>
        <StackLayout>
          <Button @tap="$modal.close" text="Close" />
        </StackLayout>
      </Page>
    </Frame>
  `,
};
const App = {
	components: {
		Items,
		Browse,
		Search,
	},

	template: `
    <BottomNavigation>
      <TabStrip>
        <TabStripItem>
          <Label text="Home"></Label>
        </TabStripItem>
        <TabStripItem>
          <Label text="Browse"></Label>
        </TabStripItem>
        <TabStripItem>
          <Label text="Search"></Label>
        </TabStripItem>
      </TabStrip>

      <TabContentItem>
        <Frame id="homeTabFrame">
          <Items />
        </Frame>
      </TabContentItem>

      <TabContentItem>
        <Frame id="browseTabFrame">
          <Browse />
        </Frame>
      </TabContentItem>

      <TabContentItem>
        <Frame id="searchTabFrame">
          <Search />
        </Frame>
      </TabContentItem>
    </BottomNavigation>
  `,
};

new Vue({
	render: (h) => h(App),
}).$start();
