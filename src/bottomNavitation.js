import Vue from "nativescript-vue";
// import Main from "./screens/App"
// import Search from "./screens/Search.vue"
// import Browse from "./screens/Browse.vue"
 import Navigator from 'nativescript-vue-navigator';
 import Vuex from 'vuex';
 import {routes} from './routes'
 import store from './store';
//  import HelloWorld from './screens/HelloWorld';
 Vue.use(Navigator, { routes })
 import Items from './screens/Modal.vue';
 import Browse from './screens/Browse.vue';

const App1 = {
  template: `
    <Page>
      <StackLayout>
        <Button text="Show Details modally" @tap="showDetailPageModally" />
      </StackLayout>
      <Navigator :defaultRoute="'/browse'"/>
    </Page>
  `,
};
const App = {
  screens: {
    Items,
    Browse,
  },

  template: `
    <Frame>
      <BottomNavigation>
        <TabStrip>
          <TabStripItem>
            <Label text="Home"></Label>
          </TabStripItem>
          <TabStripItem>
            <Label text="Browse"></Label>
          </TabStripItem>
        </TabStrip>

        <TabContentItem>
          <Frame id="homeTabFrame">
            <Items />
          </Frame>
        </TabContentItem>

        <TabContentItem>
          <Frame id="browseTabFrame">
            <Browse />
          </Frame>
        </TabContentItem>

        </TabContentItem>
      </BottomNavigation>
    </Frame>
  `
};

new Vue({
  render: (h) => h(App),
  store
}).$start();
