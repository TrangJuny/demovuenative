import Vue from "nativescript-vue";

const Master = {
	template: `
    <Page>
      <ActionBar title="Master" />
      <StackLayout>
        <Button text="To Details directly222" @tap="$navigateTo(detailPage2)" />
      </StackLayout>
    </Page>
  `,

	data() {
		return {
			detailPage: Detail,
			detailPage2: Detail2,
		};
	},
};

const Detail = {
	template: `
     <Page>
      <ActionBar title="Master" />
      <StackLayout>
        <Button text="To Details directly 2"  @tap="$navigateTo(detailPage2)" />
      </StackLayout>
    </Page>
  `,
};
const Detail2 = {
	template: `
    <Page>
      <ActionBar title="Detail2"/>
      <StackLayout>
        <Button text="To Details directly 11111111"  @tap="$navigateTo(detailPage)" />
      </StackLayout>
    </Page>
  `,
};

new Vue({
	render: (h) => h("frame", [h(Master)]),
}).$start();
