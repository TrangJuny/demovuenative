import Vue from "nativescript-vue";
import Main from "./screens/App"
import Search from "./screens/Search.vue"
import Browse from "./screens/Browse.vue"
 import Navigator from 'nativescript-vue-navigator'
 import {routes} from './routes'
 Vue.use(Navigator, { routes })

const App = {
	screens: {
		Main,
		Browse,
		Search,
	},

  template: `
  <Navigator :defaultRoute="isLoggedIn ? '/home' : '/login'"/>
    <BottomNavigation>
      <TabStrip>
        <TabStripItem>
          <Label text="Home"></Label>
        </TabStripItem>
        <TabStripItem>
          <Label text="Browse"></Label>
        </TabStripItem>
        <TabStripItem>
          <Label text="Search"></Label>
        </TabStripItem>
      </TabStrip>

      <TabContentItem>
        <Frame id="homeTabFrame">
          <Main />
        </Frame>
      </TabContentItem>

      <TabContentItem>
        <Frame id="browseTabFrame">
          <Browse />
        </Frame>
      </TabContentItem>

      <TabContentItem>
        <Frame id="searchTabFrame">
          <Search />
        </Frame>
      </TabContentItem>
    </BottomNavigation>
  `,
};

new Vue({
	render: (h) => h(App),
}).$start();
