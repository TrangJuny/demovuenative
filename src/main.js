import Vue from "nativescript-vue";
 import Navigator from 'nativescript-vue-navigator';
 import Vuex from 'vuex';
 import {routes} from './routes'
 import components from "./components/index.js";
 import store from "./store";
 import Vuelidate from "vuelidate";
//  import VuelidateMessages from "~/plugins/vuelidate/messages";
 
 Vue.use(Navigator, { routes })
 Vue.use(components);
 Vue.use(Vuelidate);
//  Vue.use(VuelidateMessages);

const App = {
  template: `
    <Page class="ns-root">
      <Navigator :defaultRoute="'/auth/login'"/>
    </Page>
  `,
};

new Vue({
  render: (h) => h(App),
  store,
}).$start();
