import Browse from './screens/Browse';
import App from './screens/App';
import Login from './screens/auth/Login';
import Auth from './screens/auth/Index';
import Bottom from "./screens/Bottom";
import Modal from "./screens/Modal";
import RegisterStep1 from "./screens/auth/RegisterStep1";
import RegisterStep2 from "./screens/auth/RegisterStep2";
import RegisterStep3 from "./screens/auth/RegisterStep3";
import RegisterSuccess from "./screens/auth/RegisterSuccess";
import ResetPassword from "./screens/auth/ResetPassword";
import ForgotPasswordSuccess from "./screens/auth/ForgotPasswordSuccess";

import ForgotPassword from "./screens/auth/ForgotPassword";

export const routes = {
  '/main': {
    component: App,
  },
  '/modal': {
    component: Modal,
  },
  '/browse': {
    component: Browse,
  },
  '/auth/login': {
    component: Login,
  },
  '/auth': {
    component: Auth,
  },
  '/auth/registerStep1': {
    component: RegisterStep1,
  },
  '/auth/registerStep2': {
    component: RegisterStep2,
  },
  '/auth/registerStep3': {
    component: RegisterStep3,
  },
  '/auth/registerSuccess': {
    component: RegisterSuccess,
  },
  '/auth/resetPassword': {
    component: ResetPassword,
  },
  '/auth/forgotPassword': {
    component: ForgotPassword,
  },
  '/auth/forgotPasswordSuccess': {
    component: ForgotPasswordSuccess,
  },
  
}