import Vue from "nativescript-vue";

export default function install() {
  const req = require.context(".", true, /\.vue$/i);
  req.keys().map((key) => {
    const match = key.match(/\w+/);

    if (match) {
      Vue.component(match[0], req(key).default);
    }
    console.log(match[0], req(key).default)
  });
}
