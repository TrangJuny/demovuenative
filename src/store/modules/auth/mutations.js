import { AUTH } from "../../../constants/mutations";

export default {
  [AUTH.SET_REGISTER_EMAIL](state, payload) {
    state.registerEmail = payload;
  },
  [AUTH.SET_REGISTER_NAME](state , payload) {
    state.registerName = payload;
  },
  [AUTH.SET_REGISTER_PASSWORD](state, payload) {
    state.registerPassword = payload;
  },
  [AUTH.SET_LOGIN_EMAIL](state, payload) {
    state.loginEmail = payload;
  },
  [AUTH.SET_FORGOT_PASSWORD_EMAIL](state, payload) {
    state.forgotPasswordEmail = payload;
  },
};
