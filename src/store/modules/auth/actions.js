import { AUTH } from "../../../constants/mutations";

export default {
  setRegisterEmail({ commit }, payload) {
    commit(AUTH.SET_REGISTER_EMAIL, payload);
  },
  setRegisterName({ commit }, payload) {
    commit(AUTH.SET_REGISTER_NAME, payload);
  },
  setRegisterPassword({ commit }, payload) {
    commit(AUTH.SET_REGISTER_PASSWORD, payload);
  },
  setLoginEmail({ commit }, payload) {
    commit(AUTH.SET_REGISTER_EMAIL, payload);
  },
  setForgotPasswordEmail({ commit }, payload) {
    commit(AUTH.SET_FORGOT_PASSWORD_EMAIL, payload);
  },
};
