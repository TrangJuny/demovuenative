import { Module } from "vuex";
import state from "./state";
import actions from "./actions";
import mutations from "./mutations";
import getters from "./getters";

const auth = {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};

export default auth;
