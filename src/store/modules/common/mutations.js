import { COMMON } from "../../../constants/mutations";

export default {
  [COMMON.SET_FOCUSED_ELEMENT](state, payload) {
    state.focusedElement = payload;
  },
  [COMMON.SET_LOADING](state, payload) {
    state.isLoading = payload;
  },
  [COMMON.SET_MODAL_BOTTOM_VISIBLE](state, payload) {
    const timeOut = payload ? 150 : 300;
    state.isShowBottomModal = payload;
    setTimeout(function () {
      state.isModalBottomVisible = payload;
    }, timeOut);
  },
  [COMMON.SET_MODAL_POPUP_VISIBLE](state, payload) {
    state.isShowPopupModal = payload;
    setTimeout(function () {
      state.isModalPopupVisible = payload;
    }, 200);
  },
};
