import { COMMON } from "../../../constants/mutations";

export default {
  setFocusedElement({ commit }, payload) {
    commit(COMMON.SET_FOCUSED_ELEMENT, payload);
  },
  setLoading({ commit }, payload) {
    commit(COMMON.SET_LOADING, payload);
  },
  setModalBottomVisible({ commit }, payload) {
    commit(COMMON.SET_MODAL_BOTTOM_VISIBLE, payload);
  },
  setModalPopupVisible({ commit }, payload) {
    commit(COMMON.SET_MODAL_POPUP_VISIBLE, payload);
  },
};
