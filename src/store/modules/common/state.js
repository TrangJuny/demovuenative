
export default () => ({
  focusedElement: null,
  isLoading: false,
  isModalBottomVisible: false,
  isModalPopupVisible: false,
  isShowBottomModal: false,
  isShowPopupModal: false,
  hasError: false,
});
