export default {
  // getKeyboardHeight: (state: any) => state.keyboardHeight,
  getModalVisible: (state) =>
    state.isModalBottomVisible || state.isModalPopupVisible,
};
