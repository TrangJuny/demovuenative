import { Module } from "vuex";
import state from "./state";
import actions from "./actions";
import mutations from "./mutations";
import getters from "./getters";

const common = {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};

export default common;
